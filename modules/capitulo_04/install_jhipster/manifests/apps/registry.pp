# Class: registry
#
# Parameters: none
#
# Actions: Instala e gerencia a aplicacao Registry do JHipster
#
# Sample Usage:
#
#   include install_jhipster::apps:registry
#
class install_jhipster::apps::registry(


  #------------------------------------
  # ATENCAO! As variaveis referenciadas sao usadas neste manifest e/ou nos arquivos de templates.
  #------------------------------------

  #Variaveis gerais
  $tmp_dir    = $install_jhipster::params::tmp_dir,

  #JHipster
  $jhipster_registry_dir   = $install_jhipster::params::jhipster_registry_dir,
  $jhipster_user           = $install_jhipster::params::jhipster_user,
  $jhipster_group          = $install_jhipster::params::jhipster_group,
  $jhipster_dir_base       = $install_jhipster::params::jhipster_dir_base,
  $jhipster_init_file      = $install_jhipster::params::jhipster_init_file,
  $jhipster_log_dir        = $install_jhipster::params::jhipster_log_dir,
  $jhipster_status_dir     = $install_jhipster::params::jhipster_status_dir,
  $jhipster_config_profile = $install_jhipster::params::jhipster_config_profile,

  #Registry
  $manage_deploy_registry = $install_jhipster::params::manage_deploy_registry,
  $registry_version       = $install_jhipster::params::registry_version,
  $registry_source_file   = $install_jhipster::params::registry_source_file,
  $registry_app_file      = $install_jhipster::params::registry_app_file,
  $registry_init_file     = $install_jhipster::params::registry_init_file,
  $registry_service       = $install_jhipster::params::registry_service,
  $registry_download      = $install_jhipster::params::registry_download,
  $registry_md5_url       = $install_jhipster::params::registry_md5_url,
  $registry_page_version  = $install_jhipster::params::registry_page_version,
  $registry_user          = $install_jhipster::params::registry_user,
  $registry_password      = $install_jhipster::params::registry_password,

  ) inherits install_jhipster::params {

  #--- Variavel(is) desta classe ---#
  $subtype_service_jhipster = 'registry'

  #Criando a estrutura de diretórios do JHipster Registry
  file { $jhipster_registry_dir:
    ensure  => 'directory',
    mode    => '755',
    owner   => $jhipster_user,
    group   => $jhipster_group,
    recurse => true,
    require => File[$jhipster_principal_dir],
  }

  #Aplicando o template para o script de inicializacao do servico registry
  file { $registry_init_file:
    ensure  => 'file',
    content => template('install_jhipster/jhipster_init/jhipster-registry.erb'),
    mode    => '755',
    owner   => $jhipster_user,
    group   => $jhipster_group,
    require => File[$jhipster_principal_dir],
  }

  #Mantendo o servico registry em execucao
  service { $registry_service:
    ensure     => 'running',
    enable     => true,
    hasrestart => true,
    hasstatus  => true,
    require    => [ File[$registry_init_file],
                    Exec['update_registry'],
                  ],
  }

  #Gerenciando a atualizacaos do registry
  if $manage_deploy_registry {
    #Aplicando o template para o script de atualizacao do servico registry
    file { "${jhipster_registry_dir}/update_registry.sh":
      ensure  => 'file',
      content => template('install_jhipster/jhipster_scripts_update/update_registry.sh.erb'),
      mode    => '755',
      owner   => $jhipster_user,
      group   => $jhipster_group,
      require => File[$jhipster_registry_dir],
    }

    exec { 'update_registry':
      command   => "${jhipster_registry_dir}/update_registry.sh; ",
      provider  => 'shell',
      logoutput => 'on_failure',
      timeout   => '14400', #equivalente a 4 horas
      path      => ['/usr/local/sbin', '/usr/local/bin','/usr/sbin','/usr/bin','/sbin','/bin'],
      require   => [ Class['install_jhipster::check_space'], 
                    File[$jhipster_principal_dir], 
                    File["${jhipster_registry_dir}/update_registry.sh"],
                   ],
    }
  }
}
