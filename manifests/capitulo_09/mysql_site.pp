node 'node1.domain.com.br' {  
  #Instalando o MySQL
  class { '::mysql::server':
    root_password           => 'livro',
    remove_default_accounts => true,
    override_options        => $override_options
  }

  #Criando um banco de dados e usuario no MySQL
  mysql::db { 'jhipster':
    user     => 'jhipster',
    password => 'livro',
    host     => 'localhost',
    grant    => ['ALL PRIVILEGES'],
  }
}
