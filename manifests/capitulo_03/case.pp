case $::operatingsystem {
  centos: { $apache = "httpd" }
  redhat: { $apache = "httpd" }
  debian: { $apache = "apache2" }
  ubuntu: { $apache = "apache2" }
  default: {
  #fail eh uma funcao padrao do Puppet
  fail("sistema operacional desconhecido")
  }
}

package {'apache':
  name   => $apache,
  ensure => 'latest',
}

