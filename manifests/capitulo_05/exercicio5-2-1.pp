node master.domain.com.br {
  #Instalando o Apache e desabilitando as assinaturas do servico
  class {'apache':
	default_vhost    => false,
	server_signature => 'Off',
	server_tokens    => 'Prod',
	trace_enable     => 'Off',
  }
  #Definindo a porta HTTP padrao do Apache
  apache::listen { '80': }

  #Configurando o uso de cifras fortes e habilitando o uso de protocolo de criptografia forte
  class { 'apache::mod::ssl':
	ssl_cipher   => 'HIGH:MEDIUM:!aNULL:!MD5:!SSLv3:!SSLv2:!TLSv1:!TLSv1.1',
 	ssl_protocol => [ 'all', '-SSLv2', '-SSLv3', '-TLSv1', '-TLSv1.1' ],
  }

  #Configurando o modulo wsgi
  class { 'apache::mod::wsgi':
	wsgi_socket_prefix => '/var/run/wsgi',
  }

  #Instalando o Puppetboard e as dependencias de pacotes
  class { 'puppetboard':
	manage_git        => 'latest',
	manage_virtualenv => 'latest',
	reports_count     => 50
  }->
  python::pip { 'Flask':
	virtualenv => '/srv/puppetboard/virtenv-puppetboard',
  }->
  python::pip { 'Flask-WTF':
	virtualenv => '/srv/puppetboard/virtenv-puppetboard',
  }->
  python::pip { 'WTForms':
	virtualenv => '/srv/puppetboard/virtenv-puppetboard',
  }->
  python::pip { 'pypuppetdb':
	virtualenv => '/srv/puppetboard/virtenv-puppetboard',
  }->
  python::pip { 'requests':
	virtualenv => '/srv/puppetboard/virtenv-puppetboard',
  }->
  python::pip { 'CommonMark':
 	virtualenv => '/srv/puppetboard/virtenv-puppetboard',
  }

  #Configurando um virtualhost para acesso ao Puppetboard via HTTPS
  class { 'puppetboard::apache::vhost':
	vhost_name => 'master.domain.com.br',
	port => 443,
	ssl => true,
  }
}
