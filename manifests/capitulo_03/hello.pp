file { 'hello':
  path    => '/tmp/hello.txt',
  ensure  => present,
  mode    => '0640',
  content => "Hello, world! Ola, mundo! \n",
}

