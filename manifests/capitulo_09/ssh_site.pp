node 'node1.domain.com.br' {  
  #Gerenciando o SSH
  class { 'ssh':
    storeconfigs_enabled => false,
    server_options       => {
      'PasswordAuthentication'          => 'yes',
      'AllowTcpForwarding'              => 'no',
      'X11Forwarding'                   => 'no',
      'Port'                            => [22, 2222],
      'Protocol'                        => '2',
      'HostKey'                         => ['/etc/ssh/ssh_host_dsa_key', 
                                            '/etc/ssh/ssh_host_ecdsa_key',
                                            '/etc/ssh/ssh_host_ed25519_key',
                                            '/etc/ssh/ssh_host_rsa_key',
                                           ],
      'UsePrivilegeSeparation'          => 'yes',
      'ServerKeyBits'                   => '1024',
      'AllowUsers'                      => 'teste livro',
      'SyslogFacility'                  => 'AUTH',
      'LogLevel'                        => 'INFO',
      'LoginGraceTime'                  => '120',
      'PermitRootLogin'                 => 'no',
      'StrictModes'                     => 'yes',
      'RSAAuthentication'               => 'yes',
      'IgnoreRhosts'                    => 'yes',
      'RhostsRSAAuthentication'         => 'no',
      'HostbasedAuthentication'         => 'no',
      'PermitEmptyPasswords'            => 'no',
      'AcceptEnv'                       => 'LANG LC_*',
      'ChallengeResponseAuthentication' => 'no',
      'X11DisplayOffset'                => '10',
      'PasswordAuthentication'          => 'yes',
      'PrintMotd'                       => 'no',
      'PrintLastLog'                    => 'yes',
      'TCPKeepAlive'                    => 'yes',
      'Subsystem'                       => 'sftp /usr/lib/openssh/sftp-server'
    },
  }
}
