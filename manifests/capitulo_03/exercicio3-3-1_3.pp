mount { '/media':
  device => '/dev/sda1',
  fstype => 'ext4',
  ensure => 'mounted',
}
