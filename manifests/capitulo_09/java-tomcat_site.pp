node 'node1.domain.com.br' {  
  #Instalando o Java
  include java

  #Baixando e instalando o Tomcat 8.5.16
  tomcat::install { '/opt/tomcat':
    source_url => 'http://archive.apache.org/dist/tomcat/tomcat-8/v8.5.16/bin/apache-tomcat-8.5.16.tar.gz'
  }

  #Informando o diretorio da instancia padrao do Tomcat
  tomcat::instance { 'default':
    catalina_home => '/opt/tomcat',
  }

  #Configurando a porta padrao do HTTP
  tomcat::config::server::connector { 'tomcat-remove-port':
    connector_ensure => 'present',
    catalina_base    => '/opt/tomcat',
    port             => '8000',
    protocol         => 'HTTP/1.1',
  }

  #Alterando a senha padrao para desligar o Tomcat
  tomcat::config::server {'shutdown-tomcat':
    catalina_base => '/opt/tomcat',
    shutdown      => 'AECIO123@',
  }
}
