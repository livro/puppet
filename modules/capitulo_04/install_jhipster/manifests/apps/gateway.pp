# Class: gateway
#
# Parameters: none
#
# Actions: Instala e gerencia a aplicacao Gateway do JHipster
#
# Sample Usage:
#
#   include install_jhipster::apps:gateway
#
class install_jhipster::apps::gateway(


  #------------------------------------
  # ATENCAO! As variaveis referenciadas sao usadas neste manifest e/ou nos arquivos de templates.
  #------------------------------------

  #Variaveis gerais
  $tmp_dir = $install_jhipster::params::tmp_dir,

  #Database
  $mysql_host            = $install_jhipster::params::mysql_host,
  $mysql_port            = $install_jhipster::params::mysql_port,
  $database_gw           = $install_jhipster::params::database_gw,
  $database_gw_user      = $install_jhipster::params::database_gw_user,
  $database_gw_password  = $install_jhipster::params::database_gw_password,

  #JHipster
  $jhipster_gw_dir         = $install_jhipster::params::jhipster_gw_dir,
  $jhipster_user           = $install_jhipster::params::jhipster_user,
  $jhipster_group          = $install_jhipster::params::jhipster_group,
  $jhipster_dir_base       = $install_jhipster::params::jhipster_dir_base,
  $jhipster_log_dir        = $install_jhipster::params::jhipster_log_dir,
  $jhipster_config_profile = $install_jhipster::params::jhipster_config_profile,

  #Registry
  $registry_user         = $install_jhipster::params::registry_user,
  $registry_password     = $install_jhipster::params::registry_password,
  $registry_page_version = $install_jhipster::params::registry_page_version,
  $registry_service      = $install_jhipster::params::registry_service,

  #Gateway
  $manage_deploy_gateway = $install_jhipster::params::manage_deploy_gateway,
  $gateway_version       = $install_jhipster::params::gateway_version,
  $gateway_source_file   = $install_jhipster::params::gateway_source_file,
  $gateway_app_file      = $install_jhipster::params::gateway_app_file,
  $gateway_init_file     = $install_jhipster::params::gateway_init_file,
  $gateway_service       = $install_jhipster::params::gateway_service,
  $gateway_download      = $install_jhipster::params::gateway_download,
  $gateway_md5_url       = $install_jhipster::params::gateway_md5_url,
  $gateway_page_version  = $install_jhipster::params::gateway_page_version,

  ) inherits install_jhipster::params {

  #--- Variavel(is) desta classe ---#
  $subtype_service_jhipster = 'gateway'

  #Criando a estrutura de diretórios do gateway
  file { $jhipster_gw_dir:
    ensure  => 'directory',
    mode    => '755',
    owner   => $jhipster_user,
    group   => $jhipster_group,
    recurse => true,
    require => File[$jhipster_principal_dir],
  }

  #Aplicando o template ao script de inicializacao do servico
  file { $gateway_init_file:
    ensure  => 'file',
    content => template('install_jhipster/jhipster_init/jhipster-microservice.erb'),
    mode    => '755',
    owner   => $jhipster_user,
    group   => $jhipster_group,
    require => File[$jhipster_principal_dir],
  }

  #Aplicando o template ao arquivo de configuracao do servico
  file { "$jhipster_gw_dir/application-${jhipster_config_profile}.yml":
    ensure  => 'file',
    content => template("install_jhipster/application-${jhipster_config_profile}.yml.erb"),
    mode    => '755',
    owner   => $jhipster_user,
    group   => $jhipster_group,
    require => File[$jhipster_principal_dir],
  }

  #Mantendo o servico gateway em execucao
  service { $gateway_service:
    ensure     => 'running',
    enable     => true,
    hasrestart => true,
    hasstatus  => true,
    require    => [File[$gateway_init_file], 
                   File["$jhipster_gw_dir/application-${jhipster_config_profile}.yml"],
                   Exec['update_gateway'],
                  ],
  }

  #Gerenciado a atualizacao do gateway
  if $manage_deploy_gateway {
    #Aplicando o template para o script de atualizacao do servico gateway
    file { "${jhipster_gw_dir}/update_gateway.sh":
      ensure  => 'file',
      content => template('install_jhipster/jhipster_scripts_update/update_gateway.sh.erb'),
      mode    => '755',
      owner   => $jhipster_user,
      group   => $jhipster_group,
      require => File[$jhipster_gw_dir],
    }

    exec { 'update_gateway':
      command   => " ${jhipster_gw_dir}/update_gateway.sh; ",
      provider  => 'shell',
      logoutput => 'on_failure',
      timeout   => '14400', #equivalente a 4 horas
      path      => ['/usr/local/sbin', '/usr/local/bin','/usr/sbin','/usr/bin','/sbin','/bin'],
      require   => [ Class['install_jhipster::check_space'], 
                    File[$jhipster_principal_dir], 
                    File["${jhipster_gw_dir}/update_gateway.sh"],
                   ],
    }
  }
}
