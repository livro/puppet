#Criando o usuario livro
user { 'livro':
  ensure   => 'present',
  comment  => 'user livro,,,',
  groups   => ['livro', 'sudo', 'vagrant',],
  home     => '/home/livro',
  #A senha eh: livro
  password => '$6$LyFI6Js1$/jopKYrVoUmfuGvfz54Q6eoZTyqf6X//WcGCQgdcqD919V..isRDr2dbCw2P2Z8V2mbIZ.miWavHu/GgRb9xC/',
  shell    => '/bin/bash',
  require  => [ Group['livro'], 
                Group['sudo'], 
                Package['sudo'],]
}

#Criando alguns grupos para o usuario livro
group { 'livro':
  ensure => present,
}

group { 'sudo':
  ensure => present,
}

#Criando o diretorio HOME do usuario livro
file { '/home/livro':
  ensure => directory,
  mode   => '0644',
  owner  => livro,
  group  => livro,
}

#Atualizando a chave GPG para instalacao de pacotes do Puppet 4 do novo repositorio
if $::operatingsystem == 'debian' or $::operatingsystem == 'ubuntu' {
  exec { "download_gpg_key":
    path      => '/bin:/usr/bin:/sbin:/usr/sbin',
    command   => "wget -q https://apt.puppetlabs.com/DEB-GPG-KEY-puppet -O /tmp/DEB-GPG-KEY-puppet;",
    require   => Package['wget'],
    logoutput => 'on_failure',
  }

  exec { "validate_gpg_key":
    path      => '/bin:/usr/bin:/sbin:/usr/sbin',
    command   => "gpg --keyid-format 0xLONG /tmp/DEB-GPG-KEY-puppet | grep -q 7F438280EF8D349F",
    require   => Exec['download_gpg_key'],
    logoutput => 'on_failure',
  }

  exec { "import_gpg_key":
    path      => '/bin:/usr/bin:/sbin:/usr/sbin',
    command   => "apt-key add /tmp/DEB-GPG-KEY-puppet; \
                  apt-get update; \
                  export PATH=/opt/puppetlabs/bin:\$PATH; \
                  echo \"PATH=/opt/puppetlabs/bin:\$PATH\" >> /etc/bash.bashrc; ",
    require   => [ Exec['download_gpg_key'], 
                   Exec['validate_gpg_key'] ],
    logoutput => 'on_failure',
  }

}
elsif $::operatingsystem == 'centos' or $::operatingsystem == 'redhat' {

  #Desabilitando o firewall
  service { 'firewalld':
    ensure => 'stopped',
    enable => 'false',
  }

  #Desabilitando temporariamente o Selinux
  #Na boxe do CentOS da PuppetLabs, o Selinux ja eh desabilitado. 
  #Mas caso nao seja, descomente o trecho abaixo
#  exec { "disable_selinux":
#    path      => '/bin:/usr/bin:/sbin:/usr/sbin',
#    command   => "setenforce Disabled",
#    logoutput => 'on_failure',
#  }

  exec { "download_gpg_key":
    path      => '/bin:/usr/bin:/sbin:/usr/sbin',
    command   => "wget -q https://yum.puppetlabs.com/RPM-GPG-KEY-puppet -O /tmp/GPG-KEY-puppet;",
    require   => Package['wget'],
    logoutput => 'on_failure',
  }

  exec { "validate_gpg_key":
    path      => '/bin:/usr/bin:/sbin:/usr/sbin',
    command   => "gpg --keyid-format 0xLONG /tmp/GPG-KEY-puppet | grep -q 7F438280EF8D349F",
    require   => Exec['download_gpg_key'],
    logoutput => 'on_failure',
  }

  exec { "import_gpg_key":
    path      => '/bin:/usr/bin:/sbin:/usr/sbin',
    command   => "rpm --import /tmp/GPG-KEY-puppet \
                  export PATH=/opt/puppetlabs/bin:\$PATH; \
                  echo \"PATH=/opt/puppetlabs/bin:\$PATH\" >> /etc/bashrc; ",
    require   => [ Exec['download_gpg_key'], 
                   Exec['validate_gpg_key'] ],
    logoutput => 'on_failure',
  }
}

#Instalando o pacote wget
package { 'wget':
  ensure  => present,
}

#Instalando o pacote wget
package { 'sudo':
  ensure  => present,
}

#Atualizando os pacotes
package { [ 'puppet-agent',
            'vim',]:
  ensure      => latest,
  configfiles => 'replace',
  require     => Exec['import_gpg_key'],
}
