# livro-puppet #

[Português]: #português
[Requisitos]: #requisitos
[Instruções]: #instruções-de-uso
[Desenvolvedor]: #desenvolvedor
[Revisor]: #revisor
[Licença]: #licença

#### Conteúdo

1. [Português][Português]
    - [Requisitos][requisitos]
    - [Instruções de uso][Instruções]
2. [Desenvolvedor][Desenvolvedor]
3. [Revisor][Revisor]
4. [Licença][Licença]

# Português

Este é o repositório de manifests e módulos usados no livro "Gerência de Configuração com Puppet", publicado pela editora Novatec. 

Para obter mais informações sobre o livro acesse o link abaixo:

http://puppet.aeciopires.com

## Requisitos

1. Adquira o livro "Gerência de Configuração com Puppet", publicado pela Novatec.
2. Acesse o site do livro (http://puppet.aeciopires.com) e baixe as máquinas virtuais para praticar os exercícios.
3. Instale o Puppet nas máquinas virtuais seguindo as instruções do livro.
4. Instale o pacote "git" nas máquinas virtuais.

## Instruções de Uso

* Baixe os códigos com o comando abaixo ou acesse a página https://gitlab.com/livro/puppet/tags e o obtenha o pacote referente a tag mais recente. 

```bash
git clone https://aeciopires@gitlab.com/livro/puppet.git
cd puppet
```

* Execute os manifests e módulos conforme as instruções citadas ao longo do livro.

## Desenvolvedor

Aécio dos Santos Pires

Site: http://aeciopires.com

## Revisor

André Luis Boni Déo

Site: http://andredeo.blogspot.com.br


## Licença

MIT License

