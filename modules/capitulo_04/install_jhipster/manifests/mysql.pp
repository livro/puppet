# Class: install_jhipster
#
#
# Parameters: none
#
# Actions: Gerencia o banco de dados MySQL
#
#   include install_jhipster::mysql
#
class install_jhipster::mysql(
  
  #------------------------------------
  # ATENCAO! As variaveis referenciadas sao usadas neste manifest e/ou nos arquivos de templates.
  #------------------------------------

  #Variaveis gerais
  $tmp_dir = $install_jhipster::params::tmp_dir,

  #Database
  $mysql_host            = $install_jhipster::params::mysql_host,
  $mysql_port            = $install_jhipster::params::mysql_port,
  $mysql_root_pass       = $install_jhipster::params::mysql_root_pass,
  $database_gw           = $install_jhipster::params::database_gw,
  $database_gw_user      = $install_jhipster::params::database_gw_user,
  $database_gw_password  = $install_jhipster::params::database_gw_password, 

  ) inherits install_jhipster::params {

  #Instalando o MySQL e criando o banco de dados do gateway
  class { '::mysql::server':
    root_password           => $mysql_root_pass,
    remove_default_accounts => true,
    override_options        => $override_options,
  }

  #Criando o banco de dados usando tipos definidos do modulo puppetlabs-mysql
  mysql_database { "${mysql_host}/${database_gw}":
    name => $database_gw,
    charset => 'utf8',
  }
  mysql_user { "${database_gw_user}@${mysql_host}":
    password_hash => mysql_password($database_gw_password),
  }
  mysql_grant { "${database_gw_user}@${mysql_host}/${database_gw}.*":
    table      => "${database_gw}.*",
    user       => "${database_gw_user}@${mysql_host}",
    privileges => ['ALL'],
  }
  
}
