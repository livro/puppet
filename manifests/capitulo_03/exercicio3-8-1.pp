#Analisando o valor de um fato do host para selecionar a opcao adequada do case
case $::operatingsystem {
  centos: { 
    case $::operatingsystemmajrelease {
      '7': {
        #Declaracao de variaveis
        $required_packages = ['yum-utils',
                              'device-mapper-persistent-data',
			      'lvm2', ]

        #Configurando o acesso ao repositorio do Docker
        exec { 'repository_docker':
          command  => 'true; \
            cd /tmp; \
            yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo;',
          provider => 'shell',
          path     => ['/usr/local/sbin', '/usr/local/bin','/usr/sbin','/usr/bin','/sbin','/bin'],
          timeout  => '14400', #tempo em segundos equivalente a 4 horas
          require  => Package[$required_packages],
        }
      }
    }
  }
  debian: { 
    case $::operatingsystemmajrelease {
      '8','9': {
        #Declaracao de variaveis
        $required_packages = ['apt-transport-https',
                              'ca-certificates',
                              'curl',
                              'gnupg2', 
                              'software-properties-common', ]

        #Configurando o acesso ao repositorio do Docker
        exec { 'repository_docker':
          command  => 'true; \
            cd /tmp; \
            curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add - ; \
            add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable"; \
            apt-get update; ',
          provider => 'shell',
          path     => ['/usr/local/sbin', '/usr/local/bin','/usr/sbin','/usr/bin','/sbin','/bin'],
          timeout  => '14400', #tempo em segundos equivalente a 4 horas
          require  => Package[$required_packages],
        }
      }
    }
  }
  ubuntu: { 
    case $::operatingsystemmajrelease {
      '16.04': {
        #Declaracao de variaveis
        $required_packages = ['apt-transport-https',
                              'ca-certificates',
                              'curl',
                              'software-properties-common', ]

        #Configurando o acesso ao repositorio do Docker
        exec { 'repository_docker':
          command  => 'true; \
            cd /tmp; \
            curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add - ; \
            add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu xenial stable"; \
            apt-get update; ',
          provider => 'shell',
          path     => ['/usr/local/sbin', '/usr/local/bin','/usr/sbin','/usr/bin','/sbin','/bin'],
          timeout  => '14400', #tempo em segundos equivalente a 4 horas
          require  => Package[$required_packages],
        }  
      }
    }  
  }
  default: {
  #fail eh uma funcao padrao do Puppet
  fail("[ERRO] Nao consigo instalar o Docker-CE neste sistema operacional.")
  }
}

#Instalando os pacotes requeridos
package { $required_packages:
  ensure  => present,
}

#Iniciando o servico Docker
service { 'docker':
  ensure  => 'running',
  require => Package['docker-ce'],
}

#Instalando a ultima versao do docker-ce
package { 'docker-ce':
  ensure  => latest,
  require => Exec['repository_docker'],
}
