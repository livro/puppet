node 'node1.domain.com.br' {  
  #Instalando o MongoDB (dependencia do Graylog)
  class { 'mongodb::globals':
    manage_package_repo => true,
  }->
  class { 'mongodb::server':
    bind_ip => ['127.0.0.1'],
  }

  #Instalando o ElasticSearch (dependencia do Graylog)
  class { 'elasticsearch':
    version      => '5.5.1',
    repo_version => '5.x',
    manage_repo  => true,
  }->
  elasticsearch::instance { 'graylog':
    config => {
      'cluster.name' => 'graylog',
      'network.host' => '127.0.0.1',
    }
  }

  #Instalando o Graylog
  class { 'graylog::repository':
    version => '2.3'
  }->
  class { 'graylog::server':
    package_version => '2.3.0-7',
    config          => {
      #a senha do root do Graylog tem que ter, pelo menos, 16 caracteres
      'password_secret' => 'senhagraylog1234567',
      #o hash SHA2 correspondente a senha. 
      #Obtenha o hash digitando o comando abaixo em outro terminal/console
      #echo -n senhagraylog1234567 | shasum -a 256
      'root_password_sha2' => '86b39f6dbf4eb6bcac4d436ce134806ffc7a9fc323f0607c232a5854b5acecf7',
    }
  }
}
