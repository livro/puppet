node 'node2.domain.com.br' {  
  #Instalando o PostgreSQL de acordo com o S.O
  case $::operatingsystem {
    centos,redhat: {   
      class { 'postgresql::globals':
        manage_package_repo => false,
        needs_initdb        => true;
      }-> class { 'postgresql::server':
        ip_mask_deny_postgres_user => '0.0.0.0/32',
        ip_mask_allow_all_users    => '127.0.0.1/32',
        listen_addresses           => 'localhost',
        ipv4acls                   => ['host all all 127.0.0.1/32 trust'],
        postgres_password          => 'TPSrep0rt!',
        port                       => 5433,
      }
    }
    debian,ubuntu: {
      class { 'postgresql::globals':
        manage_package_repo => true,
        version             => '9.5',
        datadir             => '/database',
        confdir             => '/etc/postgresql/9.5/main',
        needs_initdb        => true;
      }-> class { 'postgresql::server':
        ip_mask_deny_postgres_user => '0.0.0.0/32',
        ip_mask_allow_all_users    => '127.0.0.1/32',
        listen_addresses           => 'localhost',
        ipv4acls                   => ['host all all 127.0.0.1/32 trust'],
        postgres_password          => 'TPSrep0rt!',
        port                       => 5433,
      }
    }
  }

  #Garantindo acesso ao banco mydb pelo usuario myuser
  postgresql::server::db { 'mydb':
    user     => 'myuser',
    password => postgresql_password('myuser', 'mypass'),
  }

  #Criando o usuario myuser
  postgresql::server::role { 'myuser':
    password_hash => postgresql_password('myuser', 'mypass'),
    superuser     => true,
  }

  #Garantindo todos os privilegios de acesso ao usuario myser no banco mydb
  postgresql::server::database_grant { 'mydb':
    privilege => 'ALL',
    db        => 'mydb',
    role      => 'myuser',
  }

  postgresql::server::pg_hba_rule { 'allow application network to access app database':
    description => 'Liberando o acesso a todos os bancos para usuarios locais do postgresql',
    type        => 'local',
    database    => 'all',
    user        => 'all',
    auth_method => 'trust',
    order       => 2,
  }

}
