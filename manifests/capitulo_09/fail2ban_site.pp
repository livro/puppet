node 'node1.domain.com.br' {  
  #Instalando o Fail2ban
  class { 'fail2ban':
    config_file_template => "fail2ban/${::lsbdistcodename}/etc/fail2ban/jail.conf.erb",
  } 
}
