node 'node1.domain.com.br' {
  #Atribuindo um role ao host
  include roles::app_b
}

node 'node2.domain.com.br' {
  #Atribuindo um role ao host
  include roles::app_a
}
