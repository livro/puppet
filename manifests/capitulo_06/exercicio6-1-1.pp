#Declaracao de variaveis
$puppet_version_required = '4.0.0'
$senha_app               = 'my_app_passwd'
$password_file           = '/tmp/senha_app.txt'
$host			 = "${::fqdn}"
$net_interfaces          = "${::interfaces}"

#A funcao split usa um expressao regular para achar uma combinacao que seja suficiente para dividir uma string em varias partes criando um array
$array_hostname = split($host, '[.]')

#Funcao que imprime uma mensagem de notificacao
notice("O nome DNS deste host eh formado pelos seguintes elementos: host,dominio,tipo,pais => host=${array_hostname[0]},dominio=${array_hostname[1]},tipo=${array_hostname[2]},pais=${array_hostname[3]}")

#A funcao md5 retorna um hash MD5 para uma string passada como parametro
#A funcao sha1 retorna um hash SHA1 para uma string passada como parametro
#A seguir sera gerado um hash SHA1 para um hash MD5 retornado a partir de uma string
file {$password_file:
  ensure  => file,
  content => sha1(md5($senha_app)),
  mode    => '600',
  owner   => 'root',
  group   => 'root',
}

#A funcao versioncmp compara duas versoes. 
#Retorna 1 quando a primeira versao eh maior que a segunda
#Retorna 0 se forem iguais
#Retorna -1 se a segunda for maior que a primeira
if versioncmp($::puppetversion, $puppet_version_required) > 0 {
  notice("A versao do Puppet que esta instalada eh: ${::puppetversion}.")
}
else{
  #Funcao que imprime uma mensage de falha e encerra a execucao do catalogo
  fail("A versao do Puppet requerida eh: ${puppet_version_required}. Mas esta instalada versao ${::puppetversion}. Por favor, atualize o Puppet.")
}

#A funcao match usa uma expressao regular para localizar e filtrar parte de uma string
$interfaces_filtered=$net_interfaces.match(/[a-z]+/)
notice("Nome das interfaces de rede que nao possui numeros no nome ==> ${interfaces_filtered}")

