# Class: params
#
# Define parametros e valores padrao do modulo.
#
# Atencao: 
#
# Alguns parametros tem valores customizados de acordo com o tipo de serviço 
# oferecido pelo servidor que compoem o ambiente do JHipster.
#
# Nestes casos, os parametros recebem os valores atraves do Hiera com dados 
# armazenados em arquivos do tipo "*.yaml". Se o Hiera, nao conseguir 
# recuperar os dados, entao serao atribuidos valores padrao aos parametros.
#
# Outros parametros nao recebem valores atraves do Hiera e so devem ser 
# alterados pela equipe de desenvolvimento do modulo, pois afetam diretamente a
# logica das classes e execucao dos comandos. 
#
class install_jhipster::params {

  #Variaveis gerais
  $init_dir       = '/etc/init.d/'
  #Espaco requerido: 2 GB ou 2.000.000.000 bytes (arredondando)
  $space_required = hiera('space_required', 2000000000)
  $tmp_dir        = hiera('tmp_dir', '/tmp')

  #Atribuicoes de variaveis de acordo com a distribuicao e versao GNU/Linux 
  case $::operatingsystem {
    'redhat','centos': {
      $java_version_redhat  = '1.8.0_141'
      $distro_downcase      = 'redhat'
      $url_base_java_oracle = "http://puppet.aeciopires.com/files/java/${java_version_redhat}/${distro_downcase}/"
      $url_rpm_jdk_oracle   = "${url_base_java_oracle}/jdk-8u141-linux-x64.rpm"
      $url_rpm_jre_oracle   = "${url_base_java_oracle}/jre-8u141-linux-x64.rpm"
    }
  }

  #Databases
  $mysql_host            = hiera('mysql_host', 'localhost')
  $mysql_port            = hiera('mysql_port', '3306')
  $mysql_root_pass       = hiera('mysql_root_pass', 'livro')
  $database_gw           = hiera('database_gw', 'jhipster')
  $database_gw_user      = hiera('database_gw_user', 'root')
  $database_gw_password  = hiera('database_gw_password', 'livro')

  #JHipster
  $type_service_jhipster = hiera('type_service_jhipster', ['all',]) # Pode ser ['all'] or ['registry','gateway']
  $type_database         = hiera('type_database', 'mysql')
  $jhipster_init_file    = "${init_dir}/jhipster"
  $jhipster_name_app     = 'jhipster'
  $jhipsteradmin_user    = hiera('jhipsteradmin_user', 'jhipsteradmin')
  $jhipsteradmin_pass    = hiera('jhipsteradmin_pass', '$6$ZXIo1jsq$Z6djZeIL6/pCYvpNq8ej/2np2/2EU6GkuGsti33Y0YQsMghdKEN8fvSpXFEDAoYzX9Wx.y32LjjADL7YfX5CY0') #jhipsteradmin
  $jhipsteradmin_group   = hiera('jhipsteradmin_group', 'jhipsteradmin')
  $jhipster_user         = hiera('jhipster_user', 'jhipster')
  $jhipster_pass         = hiera('jhipster_pass', '$6$WsMKpnG4$cfjsN0IbX6IYFgE5TFuG64IdOSX8lZiBKiOvb5RTe2FX0GxEqQHqolcnatlm8pnQR6r5VHGZr23cGyqsdfHfC.') #jhipster
  $jhipster_group        = hiera('jhipster_group', 'jhipster')

  #Hierarquia de diretorio do jhipster.
  $jhipster_dir_base       = '/home/jhipster/'
  $jhipster_log_dir        = "${jhipster_dir_base}/log/"
  $jhipster_config_profile = hiera('jhipster_config_profile', 'prod')
  $jhipster_principal_dir  = ["${jhipster_dir_base}", "${jhipster_log_dir}"] 

  ############## Deploy de Apps ###################
  
  #Registry
  $manage_deploy_registry = hiera('manage_deploy_registry', true)
  $jhipster_registry_dir  = "${jhipster_dir_base}/registry/"
  $registry_download_url  = hiera('registry_download_url', 'http://puppet.aeciopires.com/files/jhipster_registry/')
  $registry_version       = hiera('registry_version', '3.0.3')
  $registry_source_file   = "jhipster-registry-${registry_version}.war"
  $registry_app_file      = hiera('registry_app_file', 'jhipster-registry.war')
  $registry_service       = hiera('registry_service', 'jhipster-registry')
  $registry_init_file     = hiera('registry_init_file', "${init_dir}/${registry_service}")
  $registry_download      = "${registry_download_url}/v${registry_version}/${registry_source_file}"
  $registry_md5_url       = "${registry_download}.md5"
  $registry_port          = hiera('registry_port', '8761')
  $registry_ip            = hiera('registry_ip', 'localhost')
  $registry_user          = hiera('registry_user', 'admin')
  $registry_password      = hiera('registry_password', 'admin')
  $registry_page_version  = "http://${registry_user}:${registry_password}@${registry_ip}:${registry_port}/management/info"

  #Gateway
  $manage_deploy_gateway = hiera('manage_deploy_gateway', true)
  $jhipster_gw_dir       = "${jhipster_dir_base}/gw/"
  $gateway_download_url  = hiera('gateway_download_url', 'http://puppet.aeciopires.com/files/jhipster-sample-application/')
  $gateway_version       = hiera('gateway_version', '0.0.1')
  $gateway_source_file   = "jhipster-sample-application-${gateway_version}.war"
  $gateway_app_file      = hiera('gateway_app_file', 'jhipster-gateway.war')
  $gateway_service       = hiera('gateway_service', 'jhipster-gateway')
  $gateway_init_file     = hiera('gateway_init_file', "${init_dir}/${gateway_service}")
  $gateway_download      = "${gateway_download_url}/${gateway_version}/${gateway_source_file}"
  $gateway_md5_url       = "${gateway_download}.md5"
  $gateway_port          = hiera('gateway_port', '8888')
  $gateway_page_version  = "http://localhost:${gateway_port}/management/info"
}
