class profiles::server::web {
  #Criando uma variavel de profile que obtera o valor do tipo String a partir do Hiera.
  #Se nao conseguir obter o valor via Hiera, recebera um valor padrao
  $http_port = lookup('profiles::server::web::http_port', {value_type => String, default_value => '80'})

  #Configurando o Apache
  class { 'apache':
    mpm_module       => 'prefork',
    default_vhost    => false,
    server_signature => 'Off',
    server_tokens    => 'Prod',
    trace_enable     => 'Off',
  }

  #Definindo a porta padrao do HTTP
  apache::listen { $http_port: }

  #Defindo as cifras e protocolos SSL a serem usados no acesso via HTTPS
  class { 'apache::mod::ssl':
    ssl_cipher   => 'HIGH:MEDIUM:!aNULL:!MD5:!SSLv3:!SSLv2:!TLSv1:!TLSv1.1',
    ssl_protocol => [ 'all', '-SSLv2', '-SSLv3', '-TLSv1', '-TLSv1.1' ],
  }
}
