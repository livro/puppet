node 'node2.domain.com.br' {  
  #Criando um certificado com openssl
  class { '::openssl':
    package_ensure         => latest,
    ca_certificates_ensure => latest,
  }

  #Propriedades do certificado
  openssl::certificate::x509 { 'node2.domain.com.br':
    ensure       => present,
    country      => 'BR',
    organization => 'Livro Puppet',
    commonname   => $fqdn,
    state        => 'Paraiba',
    locality     => 'Joao Pessoa',
    unit         => 'Livro',
    days         => 3650,
    base_dir     => '/etc/ssl/certs/',
    owner        => 'root',
    group        => 'root',
    password     => 'j(D$',
    force        => true,
  }
}
