node 'node1.domain.com.br' {  
  #Instalando o Postfix
  class { '::postfix':
    manage_conffiles => true,
    #Enviara emails apenas de aplicacoes do host local
    inet_interfaces  => '127.0.0.1, [::1]',
  }

  postfix::config { 'relay_domains':
    ensure => present,
    value  => 'localhost',
  }

  #Desativa o comando SMTP VRFY. 
  #Isso interrompe algumas tecnicas usadas para obter enderecos de email.
  postfix::config { 'disable_vrfy_command':
    ensure => present,
    value  => 'yes',
  }
}
