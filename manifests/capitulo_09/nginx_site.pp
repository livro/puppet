node 'node1.domain.com.br' {  
  #Instalando o Nginx e configurando um Proxy reverso para o site do Jhipster
  class { 'nginx': }

  #Ao acessar a porta 80 do node1, devera ser exibida a pagina do Jhipster que esta na porta 8888. 
  #Lembre-se de configurar o DNS para que o seu navegador possa acessar o host node1
  nginx::resource::server { 'node1.domain.com.br':
    listen_port => 80,
    proxy       => 'http://node1.domain.com.br:8888',
  }
}
