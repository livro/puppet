# Class: check_space
#
# Parameters: none
#
# Actions: Checa o espace requerido da particao /tmp
#
# Sample Usage:
#
#   include install_jhipster::check_space
#
class install_jhipster::check_space(

  #------------------------------------
  # ATENCAO! As variaveis referenciadas sao usadas neste manifest e/ou nos arquivos de templates.
  #------------------------------------

  #Variaveis gerais
  $tmp_dir        = $install_jhipster::params::tmp_dir,
  $space_required = $install_jhipster::params::space_required,

  ) inherits install_jhipster::params {

  # Alguns sysadmins criam o /tmp em particao separada do /
  if $::mountpoints["${tmp_dir}"] {
    $free_space = $::mountpoints["${tmp_dir}"]['available_bytes']
  }
  # Se entrar no elsif, eh porque o /tmp esta na mesma particao do /
  elsif $::mountpoints['/'] {
    $free_space = $::mountpoints['/']['available_bytes']
  }

  #Testando se ha o espaco requerido
  if $free_space > $space_required {
    notify{ 'info_free_space':
      message => "[OK] Ha espaco livre suficiente em ${tmp_dir}. Espaco requerido: ${space_required} bytes, espaco livre: ${free_space} bytes",
    }
  }
  else {
    fail("[ERRO] Espaco insuficiente na particao ${tmp_dir}. Espaco requerido: ${space_required} bytes, espaco livre: ${free_space} bytes")
  }
}
