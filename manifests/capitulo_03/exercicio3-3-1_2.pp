user { 'admin':
  ensure   => 'present',
  comment  => 'user admin,,,',
  groups   => ['adm', 'cdrom', 'dip',],
  home     => '/home/admin',
  password => '$6$LyFI6Js1$/jopKYrVoUmfuGvfz54Q6eoZTyqf6X//WcGCQgdcqD919V..isRDr2dbCw2P2Z8V2mbIZ.miWavHu/GgRb9xC/',
  shell    => '/bin/bash',
}

file { '/home/admin':
  ensure => directory,
  mode   => '0644',
  owner  => admin,
  group  => admin,
}
