exec { 'download_glassfish':
  command   => "cd /tmp; \
     wget http://puppet.aeciopires.com/files/glassfish-4.1.2.zip;",
  provider  => 'shell',
  logoutput => 'on_failure',
  timeout   => '14400', #tempo em segundos equivalente a 4 horas
  path      => ['/usr/local/sbin', '/usr/local/bin','/usr/sbin','/usr/bin','/sbin','/bin'],
}
