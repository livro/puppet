file { '/tmp/teste1.txt':
  ensure  => present,
  content => "Isso eh so mais um teste!\n",
}
file { '/tmp/teste_dir':
  ensure => directory,
  mode   => '0644',
}
file { '/tmp/teste2.txt':
  ensure => link,
  target => '/tmp/teste1.txt',
}
notify {"Gerando uma notificacao!":}
notify {"Gerenciando arquivos.":}

