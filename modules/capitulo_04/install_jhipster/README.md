# install_jhipster #

[Português]: #português
[Requisitos]: #requisitos
[Instruções]: #instruções-de-uso
[Hiera]: #hiera
[Developers]: #developers
[License]: #license

#### Conteúdo

1. [Português][Português]
    - [Requisitos][requisitos]
    - [Instruções de uso][Instruções]
    - [Hiera][Hiera]
2. [Developers][Developers]
3. [License][License]

# Português

Este é o modulo *install_jhipster* desenvolvido usando a linguagem Puppet. Ele instala e configura o jhipster, pacotes e serviços requisitos. 

## Requisitos

1. Puppet 4.x
2. Sistema operacional: Red Hat 7 e Ubuntu 16.04 (ambos 64 bits) e em inglês.
3. Módulos puppet requisitos:

* https://forge.puppetlabs.com/puppetlabs/mysql (versão 3.11.0 ou superior)
* https://forge.puppetlabs.com/puppetlabs/apt
* https://forge.puppetlabs.com/puppetlabs/stdlib

## Instruções de Uso

Para usar o módulo *install_jhipster*, é necessário:

* Copiar o diretório **install_jhipster** para a máquina **puppetserver**.
* Na máquina **puppetserver**, mover os diretórios **install_jhipster** para o diretório de módulos, por exemplo: **/etc/puppetlabs/code/environments/production/modules/**
* Ainda na máquina **puppetserver**, devem ser executados os comandos abaixo para instalação dos módulos requisitos do módulo **install_jhipster**.

~~~ puppet 
puppet module install puppetlabs-mysql
~~~ 

* Editar o aquivo **/etc/puppetlabs/code/environments/production/manifests/site.pp** e definir quais hosts usarão o módulo, conforme o exemplo abaixo.

### Exemplo da configuração do arquivo site.pp

~~~ puppet 
node "node1.domain.com.br" {
    include 'install_jhipster'
}
~~~ 

Quando a instalação do jhipster for concluída, a interface web pode ser acessada pela URL: *http://IP_SERVER:8888*.

## Hiera

O módulo *install_jhipster* instala e configura o jhipster com as configurações definidas em parâmetros ou variáveis declaradas no manifest **params.pp**. 

Algumas variáveis possuem valores customizados de acordo com o tipo de serviço oferecido pela máquina. Estes valores são obtidos através do Hiera (com dados armazenados em arquivos do tipo "*.yaml"). 

Abaixo está um exemplo do arquivo de configuração do Hiera, que deve ficar localizado em: /etc/puppetlabs/code/environments/name_environment/hiera.yaml

~~~ puppet 
---
version: 5
defaults:
  datadir: hieradata
  data_hash: yaml_data
hierarchy:
  - name: "Hosts"
    paths:
      - "host/%{::trusted.certname}.yaml"
      - "host/%{::facts.networking.fqdn}.yaml"
      - "host/%{::facts.networking.hostname}.yaml"

  - name: "Dominios"
    paths:
      - "domain/%{::trusted.domain}.yaml"
      - "domain/%{::domain}.yaml"

  - name: "Dados comuns"
    path: "common.yaml"
~~~

Dessa forma, o Hiera buscará, prioritariamente, os valores definidos nas variáveis de host (sobrepondo os valores de variáveis de mesmo nome definidas por domínio). Estas variaveis devem ficar em arquivos como esse: */etc/puppetlabs/code/environments/name_environment/hieradata/host/node1.domain.com.br.yaml*.

Caso não sejam definidos valores para variáveis nos arquivos de hosts, o Hiera buscará valores definidos em variáveis de domínio. As variáveis de domínio devem ficar em arquivos como esse: */etc/puppetlabs/code/environments/name_environment/hieradata/domain/domain.com.br.yaml*.

Mesmo que nenhum destes arquivos existam, serão aplicados os valores padrão definidos na classe **params.pp**.

### Exemplo do arquivo *.yaml para o jhipster

~~~ puppet 
#---------------------
#BEGIN-APP
#---------------------
#Deploy de Apps
#Registry
manage_deploy_registry: true
registry_download_url: 'http://puppet.aeciopires.com/files/jhipster_registry/'
registry_version: 3.0.3
registry_ip: 'localhost'
registry_port: 8761
registry_user: 'admin'
registry_password: 'admin'

#Gateway
manage_deploy_gateway: true
gateway_download_url: 'http://puppet.aeciopires.com/files/jhipster-sample-application/'
gateway_version: 0.0.1
gateway_port: 8888

#jhipster
# all, registry, gateway
type_service_jhipster:
   - 'all'
jhipster_config_profile: 'prod'

#---------------------
#FIM-APP
#---------------------

#Variaveis gerais
space_required: 2000000000
tmp_dir: '/tmp'

#Usuarios
jhipsteradmin_user: 'jhipsteradmin'
jhipsteradmin_pass: '$6$ZXIo1jsq$Z6djZeIL6/pCYvpNq8ej/2np2/2EU6GkuGsti33Y0YQsMghdKEN8fvSpXFEDAoYzX9Wx.y32LjjADL7YfX5CY0'
jhipsteradmin_group: 'jhipsteradmin'
jhipster_user: 'jhipster'
jhipster_pass: '$6$WsMKpnG4$cfjsN0IbX6IYFgE5TFuG64IdOSX8lZiBKiOvb5RTe2FX0GxEqQHqolcnatlm8pnQR6r5VHGZr23cGyqsdfHfC.'
jhipster_group: 'jhipster'

#Databases
type_database: 'mysql'
database_gw: 'jhipster'
database_gw_user: 'root'
database_gw_password: 'livro'
~~~ 

## Developers 

developer: Aecio Pires<br>
mail: aecio@dynavideo.com.br<br>

## License

Licença GPLv2 2017 Aécio dos Santos Pires
