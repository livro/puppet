#Declaracao de variaveis
$tmp_dir        = '/tmp'
#Espaco requerido: 1 GB ou 1.000 MB ou 1.000.000 bytes, arredondando a conta para simplificar
$space_required = 1000000000

#Alguns sysadmins criam o /tmp em particao separada do /
#Testando se existe a particao /tmp
if $::mountpoints["${tmp_dir}"] {
  $free_space = $::mountpoints["${tmp_dir}"]['available_bytes']
}
#Se entrar no elsif, eh porque o /tmp esta na mesma particao do /
elsif $::mountpoints['/'] {
  $free_space = $::mountpoints['/']['available_bytes']
}

#Testando se ha o espaco requerido
if $free_space > $space_required {
  notify{ 'info_free_space':
    message => "[OK] Ha espaco livre suficiente em ${tmp_dir}. Espaco requerido: ${space_required} bytes, espaco livre: ${free_space} bytes",
  }
}
else {
  fail("[ERRO] Espaco insuficiente na particao ${tmp_dir}. Espaco requerido: ${space_required} bytes, espaco livre: ${free_space} bytes")
}
