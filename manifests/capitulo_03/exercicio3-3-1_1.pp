host { 'gateway.domain.com.br':
  ensure       => 'present',
  host_aliases => ['gateway'],
  ip           => '192.168.56.254',
}

