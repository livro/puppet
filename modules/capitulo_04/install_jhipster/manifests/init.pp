# Class: install_jhipster
#
#
# Parameters: none
#
# Actions: Instala e configura o ambiente necessario ao funcionamento do jhipster
#
# Requires: Veja a lista de dependencias no arquivo README
#
# Sample Usage:
#
#   include install_jhipster
#
class install_jhipster(
  
  #------------------------------------
  # ATENCAO! As variaveis referenciadas sao usadas neste manifest e/ou nos arquivos de templates.
  #------------------------------------

  #Variaveis gerais
  $tmp_dir = $install_jhipster::params::tmp_dir,

  #JHipster
  $type_service_jhipster = $install_jhipster::params::type_service_jhipster,
  $type_database         = $install_jhipster::params::type_database,
  $jhipsteradmin_user    = $install_jhipster::params::jhipsteradmin_user,
  $jhipsteradmin_group   = $install_jhipster::params::jhipsteradmin_group,
  $jhipsteradmin_pass    = $install_jhipster::params::jhipsteradmin_pass,
  $jhipster_user         = $install_jhipster::params::jhipster_user,
  $jhipster_group        = $install_jhipster::params::jhipster_group,
  $jhipster_pass         = $install_jhipster::params::jhipster_pass,
  $jhipster_init_file    = $install_jhipster::params::jhipster_init_file,

  ) inherits install_jhipster::params {

  #----------------------DEPENDENCIAS DE MODULOS E CLASSES ----------------------#
  #O modulo puppetlabs/mysql (Leia o README para mais instrucoes)
    
  #Incluindo as classes importantes ao jhipster
  include install_jhipster::check_space
  #--------------------------------------------------------------------#

  #Verificando se o sistema operacional eh suportado
  case $::operatingsystem {
    'redhat','centos': {
      # Variaveis referentes ao Java da Oracle no RedHat
      $url_rpm_jdk_oracle  = $install_jhipster::params::url_rpm_jdk_oracle
      $url_rpm_jre_oracle  = $install_jhipster::params::url_rpm_jre_oracle
      $java_version_redhat = $install_jhipster::params::java_version_redhat

      #Instalando o Java da Oracle a partir de pacotes externos e sem interatividade para aceitar a licenca
      #O exec sera executado apenas uma vez, caso o java nao esteja instalado.
      exec { 'install_oracle_java8':
        command  => "true; \
          cd ${tmp_dir}/; \
          rpm -ivh --force ${url_rpm_jdk_oracle}; \
          rpm -ivh --force ${url_rpm_jre_oracle}; \
          echo JAVA_HOME=/usr/java/jdk${java_version_redhat}/ >> /etc/bashrc; \
          echo JRE_HOME=/usr/java/jdk${java_version_redhat}/jre >> /etc/bashrc; \
          export JAVA_HOME=/usr/java/jdk${java_version_redhat}/; \
          export JRE_HOME=/usr/java/jdk${java_version_redhat}/jre;  \
          update-alternatives --set java /usr/java/jdk${java_version_redhat}/jre/bin/java",
        onlyif   => 'aux="$(java -version 2>&1 | grep -i "java version" | cut -d \" -f2| cut -d "_" -f1)"; \
          test "$aux" != "1.8.0" ',
        provider => 'shell',
        tag      => ['java',],
        timeout  => '14400', #equivalente a 4 horas
        path     => ['/usr/local/sbin', '/usr/local/bin','/usr/sbin','/usr/bin','/sbin','/bin'],
        require  => [ Class['install_jhipster::check_space'],
                      Package['wget'],
                    ],
      }

      #Configurando o java para executar servicos em portas TCP abaixo de 1024 com usuario comum 
      exec { 'setcap_java':
        command  => "true; \
          cd ${tmp_dir}/; \
          setcap cap_net_bind_service=+ep /usr/java/jdk${java_version_redhat}/bin/java; \
          setcap cap_net_bind_service=+ep /usr/java/jdk${java_version_redhat}/jre/bin/java; ",
        provider => 'shell',
        timeout  => '14400', #equivalente a 4 horas
        path     => ['/usr/local/sbin', '/usr/local/bin','/usr/sbin','/usr/bin','/sbin','/bin'],
        require  => Exec['install_oracle_java8'],
      }

      #Aplicando o template para mapear as libs do Java no Red Hat
      file { '/etc/ld.so.conf.d/java.conf':
        ensure  => 'file',
        content => template("install_jhipster/ldconfig_java.erb"),
        mode    => '0644',
        owner   => 'root',
        group   => 'root',
      }

      #Link para a lib libjli.so
      file { '/usr/lib/libjli.so':
        ensure => 'link',
        target => "/usr/java/jdk${java_version_redhat}/lib/amd64/jli/libjli.so",
      }

      #Gerenciando pacotes e servicos
      case $::operatingsystemmajrelease {
        '7': {

          #Instalando repositorios extras
          exec { 'install_repos_redhat':
            command  => "true; \
              cd ${tmp_dir}/; \
              echo insecure >> /root/.curlrc; \
              rpm -ivh --force ftp://ftp.pbone.net/mirror/dag.wieers.com/redhat/el7/en/x86_64/dag/RPMS/rpmforge-release-0.5.3-1.el7.rf.x86_64.rpm; \
              rpm -ivh --force https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm; ",
            provider => 'shell',
            timeout  => '14400', #equivalente a 4 horas
            tag      => ['ffmpeg','java'],
            path     => ['/usr/local/sbin', '/usr/local/bin','/usr/sbin','/usr/bin','/sbin','/bin'],
            require  => Class['install_jhipster::check_space'],
          }

          #Instalando o pacotes requisitos
          package { ['git', 
                     'curl',
                     'vim',
                     'unzip',
                     'wget',
                     'gzip',
                     'bzip2', 
                     'yum-utils',
                     'jq',
                    ]:
            ensure => present,
          }
        }
        default: {
          fail('[ERRO] S.O NAO suportado.')
        }
      }
    } 
    'ubuntu': {

      #Instalando o Java da Oracle a partir de um repositorio externo e sem interatividade para aceitar a licenca
      #O exec sera executado apenas uma vez, caso o repositorio externo nao tenha sido cadastrado no APT.
      exec { 'install_oracle_java8':
        command  => "true; \
          echo \"deb http://ppa.launchpad.net/webupd8team/java/ubuntu xenial main\" | tee /etc/apt/sources.list.d/webupd8team-java.list; \
          echo \"deb-src http://ppa.launchpad.net/webupd8team/java/ubuntu xenial main\" | tee -a /etc/apt/sources.list.d/webupd8team-java.list; \
          apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys EEA14886 ; \
          apt-get update; \
          echo debconf shared/accepted-oracle-license-v1-1 select true | sudo debconf-set-selections; \
          echo debconf shared/accepted-oracle-license-v1-1 seen true | sudo debconf-set-selections; \
          apt-get -y install oracle-java8-installer oracle-java8-set-default;",
        onlyif   => 'aux="$(java -version 2>&1 | grep -i "java version" | cut -d \" -f2| cut -d "_" -f1)"; \
          test "$aux" != "1.8.0" ',
        provider => 'shell',
        tag      => ['java',],
        timeout  => '14400', #equivalente a 4 horas
        path     => ['/usr/local/sbin', '/usr/local/bin','/usr/sbin','/usr/bin','/sbin','/bin'],
        require  => [ Class['install_jhipster::check_space'],
                      Package['software-properties-common'],
                    ],
      }

      #Configurando o java para executar servicos em portas TCP abaixo de 1024 com usuario comum 
      exec { 'setcap_java':
        command  => "true; \
          cd ${tmp_dir}/; \
          setcap cap_net_bind_service=+ep /usr/lib/jvm/java-8-oracle/bin/java; \
          setcap cap_net_bind_service=+ep /usr/lib/jvm/java-8-oracle/jre/bin/java; ",
        provider => 'shell',
        timeout  => '14400', #equivalente a 4 horas
        path     => ['/usr/local/sbin', '/usr/local/bin','/usr/sbin','/usr/bin','/sbin','/bin'],
        require  => Exec['install_oracle_java8'],
      }

      #Gerenciando pacotes e servicos
      case $::operatingsystemmajrelease {
        '16.04': {

          #Instalando o pacotes requisitos
          package { ['git', 
                     'curl',
                     'vim',
                     'unzip',
                     'wget',
                     'jq',
                     'software-properties-common',
                    ]:
            ensure => present,
          }

        }
        default: {
          fail('[ERRO] S.O NAO suportado.')
        }
      }
    }
    'debian': {

      #Instalando o pacotes requisitos
      package { ['apt-transport-https',
                 'git', 
                 'curl',
                 'vim',
                 'unzip',
                 'wget',
                 'jq',
                 'software-properties-common',
               ]:
        ensure => present,
      }

      #Configurando o java para executar servicos em portas TCP abaixo de 1024 com usuario comum 
      exec { 'setcap_java':
        command  => "true; \
          cd ${tmp_dir}/; \
          setcap cap_net_bind_service=+ep /usr/lib/jvm/java-8-oracle/bin/java; \
          setcap cap_net_bind_service=+ep /usr/lib/jvm/java-8-oracle/jre/bin/java; ",
        provider => 'shell',
        timeout  => '14400', #equivalente a 4 horas
        path     => ['/usr/local/sbin', '/usr/local/bin','/usr/sbin','/usr/bin','/sbin','/bin'],
        require  => Exec['install_oracle_java8'],
      }
      
      #Gerenciando pacotes e servicos
      case $::operatingsystemmajrelease {
        '8':{

          #Instalando o Java da Oracle a partir de um repositorio externo e sem interatividade para aceitar a licenca
          #O exec sera executado apenas uma vez, caso o repositorio externo nao tenha sido cadastrado no APT.
          exec { 'install_oracle_java8':
            command  => "true; \
              echo 'deb http://ppa.launchpad.net/webupd8team/java/ubuntu trusty main' | tee /etc/apt/sources.list.d/webupd8team-java.list; \
              apt-key adv --keyserver keyserver.ubuntu.com --recv-keys EEA14886; \
              apt-get update; \
              echo debconf shared/accepted-oracle-license-v1-1 select true | sudo debconf-set-selections; \
              echo debconf shared/accepted-oracle-license-v1-1 seen true | sudo debconf-set-selections; \
              apt-get -y install oracle-java8-installer oracle-java8-set-default;",
            onlyif   => 'aux="$(java -version 2>&1 | grep -i "java version" | cut -d \" -f2| cut -d "_" -f1)"; \
              test "$aux" != "1.8.0" ',
            provider => 'shell',
            tag      => ['java',],
            timeout  => '14400', #equivalente a 4 horas
            path     => ['/usr/local/sbin', '/usr/local/bin','/usr/sbin','/usr/bin','/sbin','/bin'],
            require  => [Class['install_jhipster::check_space'],
                         Package['software-properties-common'],
                         Package['apt-transport-https'],
                        ],
          }



        }
        '9': {

          #Instalando o Java da Oracle a partir de um repositorio externo e sem interatividade para aceitar a licenca
          #O exec sera executado apenas uma vez, caso o repositorio externo nao tenha sido cadastrado no APT.
          exec { 'install_oracle_java8':
            command  => "true; \
              echo 'deb http://ppa.launchpad.net/webupd8team/java/ubuntu yakkety main' | tee /etc/apt/sources.list.d/webupd8team-java.list; \
              apt-key adv --keyserver keyserver.ubuntu.com --recv-keys EEA14886; \
              apt-get update; \
              echo debconf shared/accepted-oracle-license-v1-1 select true | sudo debconf-set-selections; \
              echo debconf shared/accepted-oracle-license-v1-1 seen true | sudo debconf-set-selections; \
              apt-get -y install oracle-java8-installer oracle-java8-set-default;",
            onlyif   => 'aux="$(java -version 2>&1 | grep -i "java version" | cut -d \" -f2| cut -d "_" -f1)"; \
              test "$aux" != "1.8.0" ',
            provider => 'shell',
            tag      => ['java',],
            timeout  => '14400', #equivalente a 4 horas
            path     => ['/usr/local/sbin', '/usr/local/bin','/usr/sbin','/usr/bin','/sbin','/bin'],
            require  => [Class['install_jhipster::check_space'],
                         Package['software-properties-common'],
                        ],
          }

        }
        default: {
          fail('[ERRO] S.O NAO suportado.')
        }
      }
    }
    default: {
      fail('[ERRO] S.O NAO suportado.')
    }
  }


  #Gerenciando o grupo do usuario admin do JHipster
  group { 'wheel':
    ensure => present,
  }

  #Gerenciando o usuario admin do JHipster
  user { $jhipsteradmin_user:
    ensure     => 'present',
    comment    => 'jhipsterAdmin',
    home       => "/home/${jhipsteradmin_user}",
    managehome => true,
    groups     => 'wheel',
    password   => $jhipsteradmin_pass,
    shell      => '/bin/bash',
    require    => Group['wheel'],
  }

  #Gerenciando o usuario comum do JHipster
  user { $jhipster_user:
    ensure     => 'present',
    comment    => 'jhipster',
    home       => "/home/${jhipster_user}",
    managehome => true,
    groups     => 'wheel',
    password   => $jhipster_pass,
    shell      => '/bin/bash',
  }

  #Alterando o nome publico do usuario Root
  user { root:
    comment => "jhipsterAdmin",
  }

  #Aplicando o arquivo de inicializacao do JHipster
  file { $jhipster_init_file:
    ensure  => 'file',
    content => template('install_jhipster/jhipster_init/jhipster.erb'),
    mode    => '755',
    owner   => 'root',
    group   => 'root',
  }

  #Criando a estrutura de diretorios do JHipster
  file { $jhipster_principal_dir:
    ensure  => 'directory',
    mode    => '755',
    owner   => $jhipster_user,
    group   => $jhipster_group,
    recurse => true,
    require => User[$jhipster_user],
  }

  #Instalando o banco de dados de acordo com o tipo escolhido
  #if $type_database == 'mysql' {
  #  include install_jhipster::mysql
  #}
  #else {
  #  fail('[ERRO] Tipo de banco de dados NÃO suportado.')
  #}

  #Instalando e configurando os servicos do JHipster de acordo com o tipo escolhido
  each( $type_service_jhipster) | String $name_service| {
    case $name_service {
      'all': {
        include install_jhipster::apps::registry
        include install_jhipster::apps::gateway
      }
      'registry': {
        include install_jhipster::apps::registry
      }
      'gateway': {
        include install_jhipster::apps::gateway
      }
      default: {
        fail('[ERRO] Tipo de servico desconhecido.')
      }
    }
  }
}
