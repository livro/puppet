node 'node1.domain.com.br' {  
  #Instalando o DHCP Server
  class { 'dhcp':
    service_ensure => running,
    dnsdomain      => ['domain.com.br'],
    nameservers    => ['8.8.8.8','4.4.4.4'],
    interfaces     => ['enp0s8'],
  }

  #Definindo um range de IP a serem atribuidos pelo DHCP
  dhcp::pool { 'dhcp.domain.com.br':
    network => '10.0.1.0',
    mask    => '255.255.255.0',
    range   => ['10.0.1.10 10.0.1.50', '10.0.1.200 10.0.1.250' ],
    gateway => '10.0.1.1',
  }

  #Atribuindo um IP fixo a um host
  dhcp::host { 'puppetserver':
    comment => 'Mac do servidor master.domain.com.br',
    mac     => '08:00:27:d0:20:f5',
    ip      => '10.0.1.51',
  }
}
