#Declaracao de variaveis
$host         = "${::hostname}"
$array_dir    = ['/','/home','/opt','/usr','/tmp','/boot']
$array_frutas = ['laranja','pera','maca','maracuja','mangaba','manga','mamao','melao','melancia']
 
#A funcao each eh eh para iteracao e usa o fato $::partition 
#para percorrer todo os elementos do array e passar como parametro
#para o lambda que exibira uma mensagem varias vezes de acordo com os elementos encontrados
each( $facts['partitions'] ) |String $partition_name, Hash $device| {
  notice("O host ${host} possui a particao ${partition_name} com tamanho igual a ${device['size']}")
}

#A funcao split usa um expressao regular para achar uma combinacao que seja suficiente para dividir o fato
#$::interfaces em um array resultante a ser usado pela funcao each, que por sua vez passara como parametro ao 
#lambda e exibira uma mensagem de acordo com o tipo de IP da interface
#A funcao sprintf ajuda na formatacao da mensagem a ser exibida pela funcao notice
split( $facts['interfaces'], ',' ).each |Integer $index, String $interface_name| {
  if( $facts["ipaddress_${interface_name}"] != '' ) {
    notice( sprintf("No host ${host} a interface %s se chama %s, com o endereco IPv4 igual a %s", $index, $interface_name, $facts["ipaddress_$interface_name"] ) )
  }

  if( $facts["ipaddress6_${interface_name}"] != '' ) {
    notice( sprintf("No host ${host} a interface %s se chama %s, com o endereco IPv6 igual a %s", $index, $interface_name, $facts["ipaddress6_$interface_name"] ) )
  }
}

#A funcao slice recebe um array e um numero inteiro que determinara como sera agrupamento dos elementos do array
notice( sprintf("Os diretorios agrupados em um array matricial (array de array): %s", slice($array_dir, 2)))

#A funcao filter filtra os elementos de um array de acordo com a expressao regular
$array_frutas_com_man = $array_frutas.filter | String $nome_fruta| { $nome_fruta =~ /man/ }
notice( sprintf("Frutas que contem 'man': %s", $array_frutas_com_man))

$array_frutas_com_mel = $array_frutas.filter | String $nome_fruta| { $nome_fruta =~ /^mel/ }
notice( sprintf("Frutas que comecam com 'mel': %s", $array_frutas_com_mel))

#A funcao with usa o array e passa como parametro ao lambda para concatenar os elementos numa unica frase.
$frase = with("A","Internet","caiu?") |$palavra1, $palavra2, $palavra3| { "${palavra1} ${palavra2} ${palavra3}" }
notice("A pergunta que voce nao gosta de ouvir eh: '${frase}'.")
