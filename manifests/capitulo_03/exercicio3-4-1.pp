#Baixando e instalando o Tomcat
exec { 'install_tomcat':
  command  => "true; \
    cd /tmp; \
    wget http://archive.apache.org/dist/tomcat/tomcat-8/v8.5.15/bin/apache-tomcat-8.5.15.tar.gz; \
    tar xzf /tmp/apache-tomcat-8.5.15.tar.gz -C /opt/tomcat; ",
  provider => 'shell',
  path     => ['/usr/local/sbin', '/usr/local/bin','/usr/sbin','/usr/bin','/sbin','/bin'],
  timeout  => '14400', #tempo em segundos equivalente a 4 horas
  require  => [Package['wget'], File['/opt/tomcat'],],
}
 
package { 'wget':
  ensure  => present,
}

file { '/opt/tomcat':
  ensure  => 'directory',
  mode    => '755',
  owner   => tomcat,
  group   => root,
  require => User['tomcat'],
}

user { 'tomcat':
  ensure => 'present',
}


